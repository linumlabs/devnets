#!/bin/sh
for filename in ./privatekeys/*; do
    geth account import --password emptypasswordfile.secret $(readlink -f $filename)
done
